# SimpleChain
SimpleChain is simple non-distributed blockchain to test how to use the data
block in a blockchain as a kind of application layer. It is a simple 
proof-of-concept tool.

SimpleChain is a simple spring-boot application that can be built using maven.
Note. POST requests are limited to 127.0.0.1 while GET are requests are not 
limited.  

## Building
The project is developed on a Linux machine with Apache Maven 3.3.9 and Java 
1.8. Please make sure both of these are installed before you attempt to run the 
project. You can verify your versions with:

    mvn --version
    java -version

## Getting the code

The latest version of the code is available on Github at
[OsloMet-ABI/simplechain](https://gitlab.com/OsloMet-ABI/simplechain).
If you haven't cloned the project then:

    git clone https://gitlab.com/OsloMet-ABI/simplechain.git

If you already have the code consider synchronizing your local copy:
    
    git fetch --all
    git checkout origin/master
    
## Maven

Please note that maven will automatically download all dependencies (jar files)
and put them in a directory ~/.m2. If you are uncomfortable with this, please
check the pom.xml files to find out which jar files will be downloaded.
 
    mvn clean install
    mvn spring-boot:run

## Running

The default profile will start a H2 database and generate the database 
structure. Note this uses *hbm2ddl.auto: create-drop* so data will be deleted
every time you start/stop the application. There is a mysql profile the 
uses *hbm2ddl.auto: validate*. You will have to create the database structure
in advance for this. For a DDL script, take a look in src/main/resources. To 
run the program with a mysql database use: 

    mvn spring-boot:run -Dspring.profiles.active=mysql


## Testing

There is small set of tests that can be run. Tests can be run on the command 
line using maven

    mvn test

The output should be similar to:

    [INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0
    [INFO] 
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESS
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 11.076 s
    [INFO] Finished at: 2019-01-28T11:02:26+01:00
    [INFO] Final Memory: 29M/328M
    [INFO] ------------------------------------------------------------------------


The tests check the following:

 1. Create 3 intertwined blocks and check that the blocks are linked
 2. Check that checksum, header and body are correct
 3. Check that it is possible to retrieve root
 4. Check that it is possible to retrieve tail
 5. Check that it is possible to retrieve  a block between root and tail
 
*We tried to test asciidoc automatic documentation for testing, but that's on
hold until we have more time*.
