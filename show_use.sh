#!/usr/bin/env bash

# Create the first block
CURL_RESPONSE=$( curl -X POST -H 'Content-Type: application/json' --data '{"header":"header example", "body": "body example"}' http://localhost:9792/simplechain/block);
echo "Create first block " $CURL_RESPONSE;

# Create the second block
CURL_RESPONSE=$( curl  -X POST -H 'Content-Type: application/json' --data '{"header":"header example", "body": "body example"}' http://localhost:9792/simplechain/block);
echo "Create second block " $CURL_RESPONSE;

# Create the second block
CURL_RESPONSE=$( curl  -X POST -H 'Content-Type: application/json' --data '{"header":"header example", "body": "body example"}' http://localhost:9792/simplechain/block);
echo "Create third block " $CURL_RESPONSE;

# Get the first block (root)
CURL_RESPONSE=$( curl  -X GET http://localhost:9792/simplechain/block/root);
echo "Get first block " $CURL_RESPONSE;

# Get the third block (tail)
CURL_RESPONSE=$( curl  -X GET http://localhost:9792/simplechain/block/tail);
echo "Get final block " $CURL_RESPONSE;

# Get the second block
CURL_RESPONSE=$( curl  -X GET http://localhost:9792/simplechain/block/087c392e61f08d80d2b8f69856b62c2000646d960f4af4c133e9e7e9af75991e38d60bfffc51b84f44aab580c3a9304a58010b478f7e4f264de134cf4ddc5c20);
echo "Get middle block " $CURL_RESPONSE;
