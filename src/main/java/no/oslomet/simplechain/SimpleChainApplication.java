package no.oslomet.simplechain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SimpleChainApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleChainApplication.class, args);
	}
}
