
package no.oslomet.simplechain.controller;

import no.oslomet.simplechain.model.APIDetails;
import no.oslomet.simplechain.model.XMLFile;
import no.oslomet.simplechain.service.IBlockService;
import no.oslomet.simplechain.service.IFileService;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

import static no.oslomet.simplechain.utils.Constants.*;

/**
 * Created by tsodring on 24/01/19.
 * <p>
 * RestController ofr root of the application
 */
@RestController
@RequestMapping(value = SLASH)
public class ApplicationController {


    private IFileService fileService;
    private IBlockService blockService;

    public ApplicationController(IFileService fileService,
                                 IBlockService blockService) {
        this.fileService = fileService;
        this.blockService = blockService;
    }

    @RequestMapping(method = RequestMethod.GET, value = FILES)
    public ResponseEntity<List<XMLFile>> getFileList() {
        return ResponseEntity.status(HttpStatus.OK).
                body(fileService.getListOfFiles());
    }

    @RequestMapping(method = RequestMethod.GET, value = FILE)
    public ResponseEntity<XMLFile> getFile(
            @PathVariable("filename") String filename)
            throws IOException {
        return ResponseEntity.status(HttpStatus.OK).
                body(fileService.getFile(filename));
    }

    @RequestMapping(method = RequestMethod.GET,
            value = SLASH + FILE + SLASH + "{filename" + "}" + SLASH +
                    "download")
    public HttpEntity<byte[]> downloadDocument(
            @PathVariable("filename") String filename)
            throws IOException {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type",
                MediaType.APPLICATION_XML_VALUE);
        String header = "Content-Disposition";
        String value = "\"attachment; filename=\"" + filename + ".xml\"";
        responseHeaders.add(header, value);
        return new HttpEntity<>(
                fileService.getFileForDownload(filename), responseHeaders);
    }

    @RequestMapping(method = RequestMethod.GET, value = GENERATE_XML)
    public ResponseEntity<XMLFile> generateXMLFile()
            throws IOException {
        return ResponseEntity.status(HttpStatus.OK).
                body(blockService.generateCopyOfChainAsXML());
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<APIDetails> getApplicationDetails() {

        APIDetails apiDetails = new APIDetails();
 /*
        Link link =
                linkTo(methodOn(BlockController.class)
                        .saveGrouseUser(null)).withRel("user");
        APIDetail createAccountDetail =
                new APIDetail(new String(link.getHref()), REL_USER, false);

        link = linkTo(methodOn(TokenEndpoint.class).postAccessToken(null, null)).withRel("login");
        APIDetail loginDetail = new APIDetail(link.getHref(), REL_LOGIN_OAUTH, false);

        link = linkTo(methodOn(OAuthController.class).logout(null)).withRel("logout");
        APIDetail logoutDetail = new APIDetail(link.getHref(), REL_LOGOUT_OAUTH, false);

        apiDetails.addApiDetal(createAccountDetail);
        apiDetails.addApiDetal(loginDetail);
        apiDetails.addApiDetal(logoutDetail);

        apiDetails.add(linkTo(methodOn
                (ApplicationController.class).
                getApplicationDetails()).withSelfRel());
*/
        return ResponseEntity.status(HttpStatus.OK).
                body(apiDetails);
    }

}
