package no.oslomet.simplechain.controller;

import no.oslomet.simplechain.model.Block;
import no.oslomet.simplechain.service.IBlockService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

import static no.oslomet.simplechain.utils.Constants.*;

/**
 * Created by tsodring on 24/01/19.
 * <p>
 * RestController for the BlockChain
 * <p>
 * From here you can get a block, a paginated list of blocks, or upload
 * a block.
 */
@RestController
@RequestMapping(value = SLASH + BLOCK)
public class BlockController {

    private IBlockService blockService;

    public BlockController(IBlockService blockService) {
        this.blockService = blockService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{checksum}")
    public ResponseEntity<Block> getBlock(
            @PathVariable("checksum") String checksum) {
        Block block = blockService.getBlock(checksum);
        return ResponseEntity.status(HttpStatus.OK)
                .body(block);
    }

    @RequestMapping(method = RequestMethod.GET, value = SLASH + ROOT)
    public ResponseEntity<Block> getRootBlock() {
        Block block = blockService.getFirstBlock();
        return ResponseEntity.status(HttpStatus.OK)
                .body(block);
    }

    @RequestMapping(method = RequestMethod.GET, value = SLASH + TAIL)
    public ResponseEntity<Block> getTailBlock() {
        Block block = blockService.getFinalBlock();
        return ResponseEntity.status(HttpStatus.OK)
                .body(block);
    }

    @RequestMapping(method = RequestMethod.GET, value = SLASH + GENERATE_XML)
    public ResponseEntity<String> generateXML()
            throws IOException {
        blockService.generateCopyOfChainAsXML();
        return ResponseEntity.status(HttpStatus.OK)
                .body("{\"status\" : \"Success\"}");
    }

    @RequestMapping(method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Block> createBlock(@RequestBody Block block) {
        blockService.createBlock(block);
        return ResponseEntity.status(HttpStatus.CREATED).
                body(block);
    }
}
