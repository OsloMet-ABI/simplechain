package no.oslomet.simplechain.model;

import org.springframework.hateoas.ResourceSupport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tsodring on 24/01/19.
 */
public class APIDetails
        extends ResourceSupport
        implements Serializable {
    protected List<APIDetail> aPIDetails = new ArrayList<>();

    public APIDetails() {
    }

    public boolean addApiDetal(APIDetail apiDetail) {
        return aPIDetails.add(apiDetail);
    }

    public List<APIDetail> getApiDetails() {
        return aPIDetails;
    }

    public void setApiDetails(List<APIDetail> aPIDetails) {
        this.aPIDetails = aPIDetails;
    }
}
