package no.oslomet.simplechain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import no.oslomet.simplechain.utils.BlockDeserializer;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

@Entity
@JsonDeserialize(using = BlockDeserializer.class)
public class Block
        extends ResourceSupport {
    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long identifier;

    @NotNull
    @Column(name = "checksum", nullable = false)
    private String checksum;

    @NotNull
    @Column(name = "header", nullable = false, length = 12000)
    private String header;

    @NotNull
    @Column(name = "body", nullable = false, length = 40000)
    private String body;

    @NotNull
    @Column(name = "created_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @OneToOne
    @JsonIgnore
    private Block parent;

    @OneToOne
    @JsonIgnore
    private Block child;

    public Long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Long identifier) {
        this.identifier = identifier;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Block getParent() {
        return parent;
    }

    public void setParent(Block parent) {
        this.parent = parent;
    }

    public Block getChild() {
        return child;
    }

    public void setChild(Block child) {
        this.child = child;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Block block = (Block) o;
        return Objects.equals(checksum, block.checksum) &&
                Objects.equals(body, block.body) &&
                Objects.equals(createdDate, block.createdDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), checksum, body, createdDate);
    }
}
