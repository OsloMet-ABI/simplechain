package no.oslomet.simplechain.model;

import org.springframework.hateoas.ResourceSupport;

import java.util.Objects;

public class XMLFile
        extends ResourceSupport {

    private Long identifier;

    private String filename;

    private Long size;

    private String createdDate;

    public Long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Long identifier) {
        this.identifier = identifier;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof XMLFile)) return false;
        if (!super.equals(o)) return false;
        XMLFile xmlFile = (XMLFile) o;
        return Objects.equals(identifier, xmlFile.identifier) &&
                Objects.equals(filename, xmlFile.filename) &&
                Objects.equals(size, xmlFile.size) &&
                Objects.equals(createdDate, xmlFile.createdDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), identifier, filename, size, createdDate);
    }

    @Override
    public String toString() {
        return "XMLFile{" +
                "identifier=" + identifier +
                ", filename='" + filename + '\'' +
                ", size=" + size +
                ", createdDate='" + createdDate + '\'' +
                '}';
    }
}
