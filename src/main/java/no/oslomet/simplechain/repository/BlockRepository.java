package no.oslomet.simplechain.repository;

import no.oslomet.simplechain.model.Block;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface BlockRepository
        extends PagingAndSortingRepository<Block, Long> {
    Optional <Block> findByChecksum(String checksum);
    Optional<Block> findTop1ByOrderByIdentifierAsc();
    Optional<Block> findTop1ByOrderByIdentifierDesc();

    Page<Block> findAll(Pageable pageable);
}
