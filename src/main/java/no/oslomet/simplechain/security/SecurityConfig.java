package no.oslomet.simplechain.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import static no.oslomet.simplechain.utils.Constants.*;

@Configuration
@EnableWebSecurity
@Order(1)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET,
                        SLASH + FILE + "/**")
                .permitAll()
                .antMatchers(HttpMethod.GET,
                        SLASH + FILES + "/**")
                .permitAll()
                .antMatchers(HttpMethod.GET,
                        SLASH + GENERATE_XML + "/**")
                .permitAll()
                .antMatchers(HttpMethod.GET,
                        SLASH + BLOCK + "/**")
                .permitAll()
                .antMatchers(HttpMethod.POST, SLASH + BLOCK + "/**")
                .hasIpAddress("127.0.0.1")
                .anyRequest().authenticated()
                .and()
                .formLogin().permitAll()
                .and()
                .csrf().disable();
    }
}
