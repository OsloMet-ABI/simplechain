package no.oslomet.simplechain.service;


import net.sf.saxon.Configuration;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import no.oslomet.simplechain.controller.BlockController;
import no.oslomet.simplechain.model.Block;
import no.oslomet.simplechain.model.XMLFile;
import no.oslomet.simplechain.repository.BlockRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.validation.constraints.NotNull;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static no.oslomet.simplechain.utils.Constants.*;
import static org.apache.commons.codec.digest.DigestUtils.sha512Hex;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by tsodring on 24/01/19.
 * <p>
 * Service class for a Block
 * <p>
 * You can get a block given a checksum or create a block
 */
@Service
public class BlockService
        implements IBlockService {

    private static final Logger logger = LoggerFactory.
            getLogger(BlockService.class);

    private BlockRepository blockRepository;
    private EntityManager entityManager;

    @Value("${simplechain.output-directory}")
    private String outputDirectory;

    public BlockService(BlockRepository blockRepository, EntityManager entityManager) {
        this.blockRepository = blockRepository;
        this.entityManager = entityManager;
    }

    /**
     * Retrieve a block identified by its checksum. Note this will either
     * throw a no entity found exception (EntityNotFoundException) or the
     * actual object. Caller can assume a non-null object.
     *
     * @return The block object in the chain, with hateoas links added
     */
    @Override
    public Block getBlock(@NotNull String checksum) {
        return setBlockLinks(getBlockOrThrow(checksum));
    }

    /**
     * Retrieve the first block in the chain. Note this will either throw a
     * no entity found exception (EntityNotFoundException) indicating the
     * chain is empty or the actual object. Caller can assume a non-null object.
     *
     * @return The first block object in the chain, with hateoas links added
     */
    @Override
    public Block getFirstBlock() {
        String hql =  "from Block AS b where b.identifier = (select min" +
                "(identifier) from Block)";

        Query query = entityManager.createQuery(hql);
        Block firstBlock = (Block)query.getSingleResult();

        if (firstBlock == null) {
            throw new EntityNotFoundException("No starting block found!");
        } else {
            return setBlockLinks(firstBlock);
        }
    }

    /**
     * Retrieve the final block in the chain. Note this will either throw a
     * no entity found exception (EntityNotFoundException) indicating the
     * chain is empty or the actual object. Caller can assume a non-null object.
     *
     * @return The final block object in the chain, with hateoas links added
     */
    @Override
    public Block getFinalBlock() {

        String hql =  "from Block AS b where b.identifier = (select max" +
                "(identifier) from Block)";

        Query query = entityManager.createQuery(hql);
        Block finalBlock = (Block)query.getSingleResult();

        if (finalBlock == null) {
            throw new EntityNotFoundException("No tail block found!");
        } else {
            return setBlockLinks(finalBlock);
        }
    }

    /**
     * Create a new block and persist to database. If the block object is
     * the first to be created, the checksum is set based solely on the
     * contained header and body. Otherwise the final block is located and
     * the incoming block is added as a child to this final block. Also add
     * hateoas links.
     *
     * @param block The incoming block object. Only header and body need to
     *              be set.
     * @return The block object persisted to the database, with hateoas links
     * added
     */
    @Override
    public Block createBlock(@NotNull Block block) {

        String hql =  "from Block AS b where b.identifier = (select min" +
                "(identifier) from Block)";

        Query query = entityManager.createQuery(hql);
        Block firstBlock = (Block)query.getSingleResult();

        String header = block.getHeader();
        String body = block.getBody();
        block.setCreatedDate(new Date());
        // Handle the first block differently
        if (firstBlock != null) {
            block.setChecksum(sha512Hex(header + body));
        } else {
            // Get last block to use as base for current block
            Block finalBlock = getFinalBlock();
            block.setChecksum(sha512Hex(
                    finalBlock.getChecksum() + header + body));
            block.setParent(finalBlock);
            finalBlock.setChild(block);
        }
        blockRepository.save(block);
        return setBlockLinks(block);
    }

    /**
     * Internal helper method. Set the hateoas links on blocks. No link will
     * be added for a missing parent or the child. The client cn assume a
     * missing rel for 'previous' identifies the block as the root and a
     * missing rel for 'next' identifies the final block
     *
     * @param block The block to use as a basis for getting next and previous
     *              links
     * @return the block with links added
     */
    private Block setBlockLinks(@NotNull Block block) {

        // Find previous block (parent)
        Block parentBlock = block.getParent();
        if (parentBlock != null) {
            block.add(linkTo(methodOn(BlockController.class).
                    getBlock(parentBlock.getChecksum()))
                    .withRel(PREVIOUS));
        }

        // Find the next block (that has current as parent)
        Block childBlock = block.getChild();
        if (childBlock != null) {
            block.add(linkTo(methodOn(BlockController.class).
                    getBlock(childBlock.getChecksum()))
                    .withRel(NEXT));
        }

        // Add a self rel
        block.add(linkTo(methodOn(BlockController.class).
                getBlock(block.getChecksum()))
                .withRel(SELF));
        return block;
    }

    public XMLFile generateCopyOfChainAsXML()
            throws IOException {

        SimpleDateFormat dateFormat = new
                SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String filename = outputDirectory + File.separator +
                dateFormat.format(new Date()) + ".xml";

        try {
            Processor p = new net.sf.saxon.s9api.Processor(new Configuration());
            Serializer s = p.newSerializer();
            s.setOutputProperty(Serializer.Property.METHOD, "xml");
            s.setOutputProperty(Serializer.Property.INDENT, "yes");
            s.setOutputStream(new FileOutputStream(filename));
            XMLStreamWriter writer = s.getXMLStreamWriter();

            Pageable pageable = PageRequest.of(0, 20,
                    Sort.by("identifier").ascending());

            writer.writeStartDocument();
            writer.writeStartElement("simplechain");


            while (true) {
                Page<Block> page = blockRepository.findAll(pageable);
                System.out.println("Page no: " + page.getNumber());

                List<Block> blocks = page.getContent();

                for (Block block : blocks) {
                    writer.writeStartElement("block");

                    writer.writeStartElement("header");
                    writer.writeCharacters(block.getHeader());
                    writer.writeEndElement();

                    writer.writeStartElement("body");
                    writer.writeCharacters(block.getBody());
                    writer.writeEndElement();

                    writer.writeStartElement("checksum");
                    writer.writeCharacters(block.getChecksum());
                    writer.writeEndElement();

                    writer.writeEndElement();
                }

                page.getContent().forEach(System.out::println);
                if (!page.hasNext()) {
                    break;
                }


                pageable = page.nextPageable();

            }
            writer.writeEndElement();
            writer.writeEndDocument();
            writer.flush();
            writer.close();

        } catch (XMLStreamException e) {
            logger.error(e.getMessage());
        } catch (SaxonApiException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return getFileDetails(new File(filename));
    }


    private XMLFile getFileDetails(File file)
            throws IOException {
        Path path = file.toPath();
        BasicFileAttributes attr =
                Files.readAttributes(path,
                        BasicFileAttributes.class);

        XMLFile xmlFile = new XMLFile();
        xmlFile.setIdentifier(0L);
        xmlFile.setCreatedDate(
                attr.creationTime().toString());
        xmlFile.setFilename(file.getName());
        xmlFile.setSize(file.length());
        return xmlFile;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid Block back. If there is no valid
     * Block, a EntityNotFoundException exception is thrown.
     *
     * @param checksum The checksum of the block to retrieve
     * @return the block
     */
    private Block getBlockOrThrow(@NotNull String checksum)
            throws EntityNotFoundException {
        return blockRepository.findByChecksum(checksum)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                "No Block exists with checksum " +
                                        checksum));
    }
}
