package no.oslomet.simplechain.service;


import no.oslomet.simplechain.controller.ApplicationController;
import no.oslomet.simplechain.model.XMLFile;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by tsodring on 12/02/19.
 * <p>
 * Service class for a File
 * <p>
 * You can get a File given a checksum or create a File
 */
@Service
public class FileService
        implements IFileService {

    private static final Logger logger = LoggerFactory.
            getLogger(FileService.class);

    @Value("${simplechain.output-directory}")
    private String outputDirectory;

    @Override
    public List<XMLFile> getListOfFiles() {

        List<XMLFile> xmlFiles = new ArrayList<>();
        File directory = new File(outputDirectory);
        File[] files = directory.listFiles();
        Arrays.sort(files,
                Comparator.comparingLong(File::lastModified).reversed());

        for (int i = 0; i < files.length; i++) {
            try {
                if (!files[i].isDirectory()) {
                    XMLFile xmlFile = new XMLFile();
                    createXMLFile(files[i], xmlFile,
                            new Long(i));
                    xmlFiles.add(xmlFile);
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
        return xmlFiles;
    }

    @Override
    public XMLFile getFile(String filename)
            throws IOException {
        XMLFile xmlFile = new XMLFile();
        File file = new File(outputDirectory + File.separator + filename);
        createXMLFile(file, xmlFile, 0L);
        return xmlFile;
    }

    public byte[] getFileForDownload(String filename)
            throws IOException {
        Path file = Paths.get(outputDirectory + File.separator + filename);
        Resource resource = new UrlResource(file.toUri());
        byte[] documentBody = new byte[(int) Files.size(file)];
        IOUtils.readFully(resource.getInputStream(), documentBody);
        return documentBody;
    }

    private void createXMLFile(File file, XMLFile xmlFile, Long identifier)
            throws IOException {
        Path path = file.toPath();
        BasicFileAttributes attr =
                Files.readAttributes(path,
                        BasicFileAttributes.class);

        xmlFile.setIdentifier(identifier);
        xmlFile.setCreatedDate(
                attr.creationTime().toString());
        xmlFile.setFilename(file.getName());
        xmlFile.setSize(file.length());

        xmlFile.add(linkTo(
                methodOn(ApplicationController.class)
                        .downloadDocument(
                                xmlFile.getFilename()))
                .withRel("download"));

        xmlFile.add(linkTo(
                methodOn(ApplicationController.class)
                        .getFile(
                                xmlFile.getFilename()))
                .withSelfRel());
    }
}
