package no.oslomet.simplechain.service;

import no.oslomet.simplechain.model.Block;
import no.oslomet.simplechain.model.XMLFile;

import javax.validation.constraints.NotNull;
import java.io.IOException;

public interface IBlockService {
    Block getBlock(@NotNull String checksum);
    Block getFirstBlock();
    Block getFinalBlock();
    Block createBlock(@NotNull Block block);

    XMLFile generateCopyOfChainAsXML() throws IOException;
}
