package no.oslomet.simplechain.service;

import no.oslomet.simplechain.model.XMLFile;

import java.io.IOException;
import java.util.List;

public interface IFileService {
    List<XMLFile> getListOfFiles();

    XMLFile getFile(String filename) throws IOException;

    byte[] getFileForDownload(String filename) throws IOException;
}
