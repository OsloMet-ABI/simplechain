package no.oslomet.simplechain.spring;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.ForwardedHeaderFilter;


/**
 * According to https://stackoverflow.com/questions/30020188/how-to-configure-spring-hateoas-behind-proxy
 * there is a requirement to add a ForwardHeader filter.
 */
@Configuration
public class ForwarderConfiguration {
    @Bean
    FilterRegistrationBean<ForwardedHeaderFilter> forwardedHeaderFilter() {
        FilterRegistrationBean<ForwardedHeaderFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new ForwardedHeaderFilter());
        return bean;
    }
}
