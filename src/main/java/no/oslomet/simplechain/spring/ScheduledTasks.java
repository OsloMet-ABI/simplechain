package no.oslomet.simplechain.spring;


import no.oslomet.simplechain.service.IBlockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ScheduledTasks {

    private static final Logger logger =
            LoggerFactory.getLogger(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat =
            new SimpleDateFormat("HH:mm:ss");
    private IBlockService blockService;

    public ScheduledTasks(IBlockService blockService) {
        this.blockService = blockService;
    }

    /**
     * Schedule generation of XML version of blockchain every
     * day at 1900.
     */
    @Scheduled(cron = "0 0 19 * * *")
    public void generateXMLCopyOfChain() {
        logger.info("Generating XML File at time {}",
                dateFormat.format(new Date()));
        try {

            blockService.generateCopyOfChainAsXML();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
}
