package no.oslomet.simplechain.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import no.oslomet.simplechain.model.Block;

import java.io.IOException;

import static no.oslomet.simplechain.utils.Constants.BODY;
import static no.oslomet.simplechain.utils.Constants.HEADER;

/**
 * Created by tsodring on 24/01/19.
 * <p>
 * Deserialiser for a block object
 * <p>
 * An incoming block object will only see body and header deserialised.
 */

public class BlockDeserializer
        extends JsonDeserializer {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Block deserialize(JsonParser jsonParser, DeserializationContext dc)
            throws IOException {

        Block block = new Block();
        ObjectNode objectNode = mapper.readTree(jsonParser);

        // Deserialize header
        JsonNode currentNode = objectNode.get(HEADER);
        if (currentNode != null) {
            block.setHeader(currentNode.textValue());
        }

        // Deserialize body
        currentNode = objectNode.get(BODY);
        if (currentNode != null) {
            block.setBody(currentNode.toString());
        }

        return block;
    }
}
