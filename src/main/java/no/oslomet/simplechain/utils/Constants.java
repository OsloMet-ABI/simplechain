package no.oslomet.simplechain.utils;

/**
 * Created by tsodring on 24/01/19.
 *
 * List of constants used in the application
 */
public final class Constants {
    public static final String SLASH = "/";
    public static final String BLOCK = "block";
    public static final String PREVIOUS = "previous-block";
    public static final String NEXT = "next-block";
    public static final String SELF = "self";
    public static final String REL = "rel";
    public static final String HREF = "href";
    public static final String LINKS = "links";
    public static final String HEADER = "header";
    public static final String BODY = "body";
    public static final String ROOT = "root";
    public static final String TAIL = "tail";
    public static final String CHECKSUM = "checksum";
    public static final String GENERATE_XML = "generate-xml";
    public static final String FILES = "files";
    public static final String FILE = "file";
    public static final String TEST_BLOCK_1_BODY = "body 1";
    public static final String TEST_BLOCK_2_BODY = "body 2";
    public static final String TEST_BLOCK_3_BODY = "body 3";
    public static final String TEST_BLOCK_1_HEADER = "header 1";
    public static final String TEST_BLOCK_2_HEADER = "header 2";
    public static final String TEST_BLOCK_3_HEADER = "header 3";
    public static final String TEST_BLOCK_1_CHECKSUM = "d2f1d68b813b4c0e93eccaa40c8e1ff9432c8286c2c2d482b1115c233e36e281ed18115175aab2ed3ac8d00f2cd65a9ec49d0e479c96f1f2626f51c0811dd3c2";
    public static final String TEST_BLOCK_2_CHECKSUM = "8207657678f55ad27c64f106c7e24b52ad07ae2b939f8126722faa1e9490341b0171987b92d575a2553e6bb7a886e901271e45a92e1acfb06cfc102d3e0a099c";
    public static final String TEST_BLOCK_3_CHECKSUM = "ffca31d784ad689729048a07d3fbedfcc5952bdd477269dd81a6f2117b546ae3d88ea0049ec3d8ef63be10f4a6469b644e01cac24cb4329a3165c4dfaa820e4b";
}
