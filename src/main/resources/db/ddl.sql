GRANT ALL ON simplechain.* to 'INSERT-USERNAME-HERE'@'%'
IDENTIFIED BY 'INSERT-PASSWORD-HERE';

FLUSH PRIVILEGES;

CREATE DATABASE simplechain;

USE simplechain;

CREATE TABLE `blocks` (
  `id`           bigint(20)     NOT NULL,
  `body`         varchar(40000) NOT NULL,
  `checksum`     varchar(255)   NOT NULL,
  `created_date` datetime       NOT NULL,
  `header`       varchar(12000) NOT NULL,
  `parent_id`    bigint(20) DEFAULT NULL,
  `child_id`     bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `PARENT_ID` (`parent_id`),
  KEY `CHILD_ID` (`child_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

ALTER TABLE blocks
  ADD CONSTRAINT CHILD_CONSTRAINT FOREIGN KEY (child_id) REFERENCES blocks (id);

ALTER TABLE blocks
  ADD CONSTRAINT PARENT_CONSTRAINT FOREIGN KEY (parent_id) REFERENCES blocks (id);

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

INSERT INTO `hibernate_sequence`
VALUES (1);
