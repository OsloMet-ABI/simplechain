package no.oslomet.simplechain;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import no.oslomet.simplechain.model.Block;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Iterator;

import static no.oslomet.simplechain.utils.Constants.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by tsodring on 24/01/19.
 * <p>
 * Single set of tests to checks the following:
 * <p>
 * 1. Create 3 intertwined blocks and check that the blocks are linked
 * 2. Check that checksum, header and body are correct
 * 3. Check that it is possible to retrieve root
 * 4. Check that it is possible to retrieve tail
 * 5. Check that it is possible to retrieve  a block between root and tail
 */
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@SpringBootTest(classes = SimpleChainApplication.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class SimpleChainApplicationTests {

    private MockMvc mockMvc;

    private static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration(restDocumentation))
                .alwaysDo(document("{method-name}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())))
                .build();
    }

    @Test
    public void createBlock() throws Exception {

        Block block = new Block();
        block.setBody(TEST_BLOCK_1_BODY);
        block.setHeader(TEST_BLOCK_1_HEADER);

        // Create First block
        ResultActions actionsBlock1 = mockMvc.perform(
                post(SLASH + BLOCK)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(asJsonString(block))
                        .accept(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.body")
                        .value(TEST_BLOCK_1_BODY))
                .andExpect(jsonPath("$.header")
                        .value(TEST_BLOCK_1_HEADER))
                .andExpect(jsonPath("$.checksum")
                        .value(TEST_BLOCK_1_CHECKSUM))
                .andExpect(content()
                        .contentType(APPLICATION_JSON_UTF8_VALUE))
                .andDo(document("root",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())
                ));

        String selfHrefBlock1 = getSelfHref(actionsBlock1);

        block.setBody(TEST_BLOCK_2_BODY);
        block.setHeader(TEST_BLOCK_2_HEADER);
        // Create second block
        ResultActions actionsBlock2 = mockMvc.perform(
                post(SLASH + BLOCK)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(asJsonString(block))
                        .accept(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.body")
                        .value(TEST_BLOCK_2_BODY))
                .andExpect(jsonPath("$.header")
                        .value(TEST_BLOCK_2_HEADER))
                .andExpect(jsonPath("$.checksum")
                        .value(TEST_BLOCK_2_CHECKSUM))
                .andExpect(content()
                        .contentType(APPLICATION_JSON_UTF8_VALUE))
                .andDo(document("root",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())
                ));

        String selfHrefBlock2 = getSelfHref(actionsBlock2);

        // Check that the previous rel/href of the second block points to the
        // first block
        String previousHrefBlock2 = getPreviousHref(actionsBlock2);
        assertThat(selfHrefBlock1, is(equalTo(previousHrefBlock2)));

        block.setBody(TEST_BLOCK_3_BODY);
        block.setHeader(TEST_BLOCK_3_HEADER);
        // Create a third block
        ResultActions actionsBlock3 = mockMvc.perform(
                post(SLASH + BLOCK)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(asJsonString(block))
                        .accept(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.body")
                        .value(TEST_BLOCK_3_BODY))
                .andExpect(jsonPath("$.header")
                        .value(TEST_BLOCK_3_HEADER))
                .andExpect(jsonPath("$.checksum")
                        .value(TEST_BLOCK_3_CHECKSUM))
                .andExpect(content()
                        .contentType(APPLICATION_JSON_UTF8_VALUE))
                .andDo(document("root",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())
                ));

        // Check that the previous rel/href of the third block points to the
        // second block
        String previousHrefBlock3 = getPreviousHref(actionsBlock3);
        assertThat(selfHrefBlock2, is(equalTo(previousHrefBlock3)));

        // Check that it's possible to get the root of the chain
        mockMvc.perform(
                get(SLASH + BLOCK + SLASH + ROOT)
                        .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.body")
                        .value(TEST_BLOCK_1_BODY))
                .andExpect(jsonPath("$.header")
                        .value(TEST_BLOCK_1_HEADER))
                .andExpect(jsonPath("$.checksum")
                        .value(TEST_BLOCK_1_CHECKSUM))
                .andExpect(content()
                        .contentType(APPLICATION_JSON_UTF8_VALUE))
                .andDo(document("root",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())
                ));

        // Check that it's possible to get the tail of the chain
        mockMvc.perform(
                get(SLASH + BLOCK + SLASH + TAIL)
                        .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.body")
                        .value(TEST_BLOCK_3_BODY))
                .andExpect(jsonPath("$.header")
                        .value(TEST_BLOCK_3_HEADER))
                .andExpect(jsonPath("$.checksum")
                        .value(TEST_BLOCK_3_CHECKSUM))
                .andExpect(content()
                        .contentType(APPLICATION_JSON_UTF8_VALUE))
                .andDo(document("root",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())
                ));

        // Check that it's possible to get a block in between root and tail of
        // the chain
        mockMvc.perform(
                get(SLASH + BLOCK + SLASH +
                        getChecksum(actionsBlock2))
                        .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.body")
                        .value(TEST_BLOCK_2_BODY))
                .andExpect(jsonPath("$.header")
                        .value(TEST_BLOCK_2_HEADER))
                .andExpect(jsonPath("$.checksum")
                        .value(TEST_BLOCK_2_CHECKSUM))
                .andExpect(content()
                        .contentType(APPLICATION_JSON_UTF8_VALUE))
                .andDo(document("root",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())
                ));
    }

    private String getHref(ResultActions actions, String blockType)
            throws IOException {
        MockHttpServletResponse response = actions.andReturn().getResponse();
        String content = response.getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(content);
        JsonNode linksNode = rootNode.path(LINKS);

        Iterator<JsonNode> elements = linksNode.elements();
        while (elements.hasNext()) {
            JsonNode link = elements.next();
            JsonNode relNode = link.get(REL);
            if (relNode.asText().equalsIgnoreCase(blockType)) {
                JsonNode hrefNode = link.get(HREF);
                return hrefNode.asText();
            }
        }
        return null;
    }

    private String getChecksum(ResultActions actions)
            throws IOException {
        MockHttpServletResponse response = actions.andReturn().getResponse();
        String content = response.getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(content);
        JsonNode checksumNode = rootNode.path(CHECKSUM);
        return checksumNode.asText();
    }

    private String getSelfHref(ResultActions actions)
            throws IOException {
        return getHref(actions, SELF);
    }

    private String getNextHref(ResultActions actions)
            throws IOException {
        return getHref(actions, NEXT);
    }

    private String getPreviousHref(ResultActions actions)
            throws IOException {
        return getHref(actions, PREVIOUS);
    }
}
