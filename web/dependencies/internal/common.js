/**
 * Created by tsodring on 28/01/19.
 */

var baseUrl = 'http://localhost:9792/simplechain/';
var baseUrlRoot = 'http://localhost:9792/simplechain/block/root';
var baseUrlTail = 'http://localhost:9792/simplechain/block/tail';
var baseUrlFiles = 'http://localhost:9792/simplechain/files';

var REL_PREVIOUS = 'previous-block';
var REL_NEXT = 'next-block';

var SetBlock = function (block) {
  localStorage.setItem("block", JSON.stringify(block));
  console.log("Setting block=" + JSON.stringify(t));
};

var GetBlock = function () {
  var block = localStorage.getItem("block");
  console.log("Getting block=" + block);
  return block;
};
