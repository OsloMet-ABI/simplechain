var app = angular.module('simplechain-app', []);

/**
 *
 */

var blockController = app.controller('BlockController', ['$scope', '$http',
  function ($scope, $http) {

    $scope.filesView = false;
    $scope.chainView = true;
    $scope.showBody = true;
    $scope.showChecksum = false;

    $http({
      url: baseUrlFiles,
      method: "GET"
    }).then(function successCallback(response) {
      console.log("GET on file root  [" + baseUrlFiles + "] returned "
        + JSON.stringify(response.data));
      $scope.fileList = response.data;
    });


    var block = GetBlock();


    // No block exists locally, go fetch tail block
    if (block == null) {
      $http({
        url: baseUrlTail,
        method: "GET"
      }).then(function successCallback(response) {
        response.data.index = 2;
        console.log("GET on tail [" + baseUrlTail + "] returned "
          + JSON.stringify(response.data));
        $scope.blockList = [];
        $scope.doLoadBodyView(response.data);
        for (var rel in response.data.links) {
          var relation = response.data.links[rel].rel;
          var hrefPrevious = response.data.links[rel].href;
          // Get the previous node
          if (relation === REL_PREVIOUS) {
            $http({
              url: hrefPrevious,
              method: "GET"
            }).then(function successCallback(responsePrevious) {
              responsePrevious.data.index = 1;
              $scope.doLoadBodyView(responsePrevious.data);
              console.log("GET on tail-previous [" + hrefPrevious + "] returned "
                + JSON.stringify(responsePrevious.data));
              // When retrieving the tail node, there is no next node so we
              // have to get two previous nodes
              for (var rel in responsePrevious.data.links) {
                var relation = responsePrevious.data.links[rel].rel;
                // Get the Previous node
                var hrefPreviousPrevious = responsePrevious.data.links[rel].href;
                if (relation === REL_PREVIOUS) {
                  $http({
                    url: hrefPreviousPrevious,
                    method: "GET"
                  }).then(function successCallback(responsePreviousPrevious) {
                    responsePreviousPrevious.data.index = 0;
                    $scope.doLoadBodyView(responsePreviousPrevious.data);
                    console.log("GET on tail-previous-previous [" +
                      hrefPreviousPrevious + "] returned "
                      + JSON.stringify(responsePreviousPrevious.data));
                    $scope.blockList.push(responsePreviousPrevious.data);
                    console.log("Add responsePrevious " + JSON.stringify(responsePrevious.data));
                    $scope.blockList.push(responsePrevious.data);
                    console.log("Add response " + JSON.stringify(response.data));
                    $scope.blockList.push(response.data);
                  });
                }
              }
            });
          }
        }
      });
    }
    else {
      for (var rel in block.links) {
        var relation = block.links[rel].rel;
        // Get the previous node
        if (relation === REL_PREVIOUS) {
          $http({
            url: block.links[rel].href,
            method: "GET"
          }).then(function successCallback(responsePrevious) {
            console.log("GET on block-previous [" + block.links[rel].href + "] returned "
              + JSON.stringify(responsePrevious.data));
            $scope.blockList.push(responsePrevious.data);
            $scope.doLoadBodyView(responsePrevious.data);
            $scope.blockList.push(block);
            $scope.doLoadBodyView(block);
          });
        }
        else if (relation === REL_NEXT) {
          $http({
            url: block.links[rel].href,
            method: "GET"
          }).then(function successCallback(responseNext) {
            console.log("GET on root-next [" + block.links[rel].href + "] returned "
              + JSON.stringify(responseNext.data));
            $scope.doLoadBodyView(responseNext.data);
            $scope.blockList.push(responseNext.data);
          });
        }
      }
      console.log("printing " + JSON.stringify($scope.blockList));
    }

    $scope.nextBlockSelected = function (links) {
      var urlNextBlock = "";
      var urlPreviousBlock = "";

      for (var rel in links) {
        if (links[rel].rel === REL_NEXT) {
          urlNextBlock = links[rel].href;
        }
      }

      // Get the next block
      $http({
        url: urlNextBlock,
        method: "GET"
      }).then(function successCallback(response) {
        console.log("GET on block [" + urlPreviousBlock + "] returned "
          + JSON.stringify(response.data));
        $scope.blockList = [];

        for (var rel in response.data.links) {
          if (response.data.links[rel].rel === REL_NEXT) {
            urlNextBlock = response.data.links[rel].href;
          }
          if (response.data.links[rel].rel === REL_PREVIOUS) {
            urlPreviousBlock = response.data.links[rel].href;
          }
        }
        $http({
          url: urlPreviousBlock,
          method: "GET"
        }).then(function successCallback(responsePrevious) {
          responsePrevious.data.index = 0;
          console.log("GET on block-previous [" + urlPreviousBlock + "] returned "
            + JSON.stringify(responsePrevious.data));
          $scope.blockList.push(responsePrevious.data);
          $scope.doLoadBodyView(responsePrevious.data);
          response.data.index = 1;
          $scope.blockList.push(response.data);
          $scope.doLoadBodyView(response.data);
          $http({
            url: urlNextBlock,
            method: "GET"
          }).then(function successCallback(responseNext) {
            responseNext.data.index = 2;
            console.log("GET on root-next [" + urlNextBlock + "] returned "
              + JSON.stringify(responseNext.data));
            $scope.blockList.push(responseNext.data);
            $scope.doLoadBodyView(responseNext.data);
          });
        });
      });
      console.log("printing " + JSON.stringify($scope.blockList));
    };

    $scope.previousBlockSelected = function (links) {

      var urlNextBlock = "";
      var urlPreviousBlock = "";

      for (var rel in links) {
        if (links[rel].rel === REL_PREVIOUS) {
          urlPreviousBlock = links[rel].href;
        }
      }

      // Get the previous block
      $http({
        url: urlPreviousBlock,
        method: "GET"
      }).then(function successCallback(response) {
        console.log("GET on block [" + urlPreviousBlock + "] returned "
          + JSON.stringify(response.data));
        $scope.blockList = [];

        for (var rel in response.data.links) {
          if (response.data.links[rel].rel === REL_NEXT) {
            urlNextBlock = response.data.links[rel].href;
          }
          if (response.data.links[rel].rel === REL_PREVIOUS) {
            urlPreviousBlock = response.data.links[rel].href;
          }
        }
        $http({
          url: urlPreviousBlock,
          method: "GET"
        }).then(function successCallback(responsePrevious) {
          responsePrevious.data.index = 0;
          console.log("GET on block-previous [" + urlPreviousBlock + "] returned "
            + JSON.stringify(responsePrevious.data));
          $scope.blockList.push(responsePrevious.data);
          $scope.doLoadBodyView(responsePrevious.data);
          response.data.index = 1;
          $scope.blockList.push(response.data);
          $scope.doLoadBodyView(response.data);
          $http({
            url: urlNextBlock,
            method: "GET"
          }).then(function successCallback(responseNext) {
            responseNext.data.index = 2;
            console.log("GET on root-next [" + urlNextBlock + "] returned "
              + JSON.stringify(responseNext.data));
            $scope.blockList.push(responseNext.data);
            $scope.doLoadBodyView(responseNext.data);
          });
        });
      });
      console.log("printing " + JSON.stringify($scope.blockList));
    };

    $scope.browseChainView = function () {
      $scope.filesView = false;
      $scope.chainView = true;
    };

    $scope.downloadChainView = function () {
      $scope.filesView = true;
      $scope.chainView = false;
    };

    $scope.doDownloadChainFile = function (file) {

      // GET REL for download document
      var hrefForDownload = "";

      for (var rel in file.links) {
        if (file.links[rel].rel === "download") {
          hrefForDownload = file.links[rel].href;
        }
      }

      console.log("file object is [" + JSON.stringify(file) + "]");
      console.log("Setting hrefForDownload to [" + hrefForDownload + "]");

      $http({
        method: 'GET',
        url: hrefForDownload,
        responseType: 'arraybuffer'
      }).then(function successCallback(response) {

        var file = new Blob([response.data], {
          type: 'application/xml'
        });

        var fileURL = URL.createObjectURL(file);
        var a = document.createElement('a');
        a.href = fileURL;
        a.target = '_blank';
        a.download = file.filename;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);

      }, function errorCallback(response) {
        console.log(" Problem downloading XML file [" + file.filename +
          "] returned " + JSON.stringify(response));
      });
    };


    $scope.doLoadChecksumView = function (block) {
      block.showChecksum = true;
      block.showHeader = false;
      block.showBody = false;
    };

    $scope.doLoadHeaderView = function (block) {
      block.showChecksum = false;
      block.showHeader = true;
      block.showBody = false;
    };

    $scope.doLoadBodyView = function (block) {
      block.showChecksum = false;
      block.showHeader = false;
      block.showBody = true;
    };
  }])
;

